from django.contrib import admin
from .models import User

# Register your models here.
class QuestionUser(admin.ModelAdmin):
   fields =['name','fruit']

admin.site.register(User, QuestionUser)