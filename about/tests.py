from django.test import TestCase, Client
from django.urls import resolve, reverse    
from .views import index, form
from .models import User

# Create your tests here.

class aboutTest(TestCase):
    def test_apakah_ada_about_page(self):
        response=Client().get('/about/')
        self.assertEqual(response.status_code,200)
    def test_apakah_ada_menggunakan_template_about(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response,'about/indexx.html')
    def test_fungsi_about(self):
        found = resolve('/about/')
        self.assertEqual(found.func,index)
    def test_apakah_ada_form_page(self):
        response=Client().get('/form/')
        self.assertEqual(response.status_code,200)
    def test_apakah_ada_menggunakan_template_form(self):
        response = Client().get('/form/')
        self.assertTemplateUsed(response,'about/form.html')
    def test_fungsi_form(self):
        found = resolve('/form/')
        self.assertEqual(found.func,form)
    def test_model_add_member(self):
        member= User.objects.create(name="tes",role="Front-End Developer")
        count_member = User.objects.all().count()
        self.assertEqual(count_member,1)
    
    