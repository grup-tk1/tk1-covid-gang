from django.shortcuts import render

# Create your views here.
from django.http import HttpResponseRedirect
from django.views.generic import FormView  
from .models import User 
from .forms import UserForm


def index(request):
    objs = User.objects.all()
    return render(request, 'about/indexx.html',{'objs':objs})
def form(request):
    response={ 'form':UserForm}
    return render(request, 'about/form.html',response)


def savekegi(request):
    form = UserForm(request.POST or None)
    f = form.save()
    if(form.is_valid and request.method == 'POST'):
        f.save()
        return HttpResponseRedirect('/about')
    else:
        return HttpResponseRedirect('/form')
