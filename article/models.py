from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from authentication.models import Account

# Create your models here.

class Article(models.Model):
    author = models.ForeignKey(Account, on_delete=models.CASCADE)
    topik = models.CharField(max_length=100)
    judul = models.CharField(max_length=100)
    image = models.CharField(max_length=400)
    date = models.DateField(auto_now_add=True)
    content = models.TextField()

class ArticleReview(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    score = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
