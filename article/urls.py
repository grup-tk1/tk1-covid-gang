from django.urls import path

from . import views

app_name = "article"

urlpatterns = [
    path('', views.index, name='article'),
    path('detail/<str:pk>/', views.detail, name='detail'),
    path('add/', views.add, name='add'),
    path('get_all/', views.get_articles, name='get'),
    path('search/result/', views.search, name="search_has_query"),
    path('search/', views.get_search, name="search")
]
