from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from .models import Article, ArticleReview
from . import forms
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from authentication.models import Account
from django.contrib.auth.decorators import login_required

import math
# Create your views here.


def index(request):
    VarA = forms.formulir()
    VarA_dictio = {
        'formulir' : VarA,
    }
    return render(request, 'article/index.html', VarA_dictio)


def detail(request, pk):
    article = Article.objects.get(id=pk)
    context = {'article' : article}
    return render(request, 'article/detail.html', context)

def add(request):
    if request.method == "POST":
        form = forms.formulir({
            'author' : request.user,
            'topik' : request.POST['topik'],
            'judul' : request.POST['judul'],
            'image' : request.POST['image'],
            'content' : request.POST['content'],
        })
        if (form.is_valid()):
            artikel = form.save()
            return redirect("/article/detail/" + str(artikel.id))
        return JsonResponse({"status":"failed"})
        # if(form.is_valid()):
        #     form.save()
        # return JsonResponse({'status' : 'success'})


def get_articles(request):
    articles = Article.objects.all()
    article_list = serializers.serialize('json', articles)
    return HttpResponse(article_list, content_type="text/json-comment-filtered")


def search(request):
    DEFAULT_PAGINATION = 20
    DEFAULT_PAGES_INTERVAL = 5
    DEFAULT_NEXT_INTERVAL = 3
    if request.method == "POST":
        search_query = request.POST["search_query"]
        current_page = int(request.POST["current_page"])
        artikel_qs = Article.objects.filter(author__username__contains=search_query) | Article.objects.filter(topik__contains=search_query) | Article.objects.filter(judul__contains=search_query)
        max_qs = artikel_qs.count()
        pages_max = math.ceil(max_qs / DEFAULT_PAGINATION)
        if pages_max < 5:
            accessible_pages = list(range(1,pages_max+1))
        elif(current_page <=3):
            accessible_pages = list(range(1,6))
        elif (current_page > pages_max - DEFAULT_PAGES_INTERVAL):
            accessible_pages = list(range(pages_max-DEFAULT_PAGES_INTERVAL+1, pages_max+1))
        else:
            accessible_pages = list(range(current_page, current_page +  DEFAULT_PAGES_INTERVAL))
        artikel_qs = artikel_qs[((current_page-1)*DEFAULT_PAGINATION):(current_page*DEFAULT_PAGINATION)]
        if artikel_qs.count() > DEFAULT_PAGINATION:
            max_qs = DEFAULT_PAGINATION
        return render(request, "article/result.html", {"artikels":artikel_qs, "max_qs":max_qs, "pages_max":pages_max, "current_page":current_page, "accessible_pages":accessible_pages, "search_query":search_query})
    return redirect('/article/search/')

def get_search(request):
    return render(request, "article/search.html")