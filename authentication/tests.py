from django.test import TestCase, Client
from django.urls import resolve
from .views import login_view, register_view
from .models import Account

# Create your tests here.
class AuthenticationTest(TestCase):

    # URL Routing Test
    def test_authentication_register_url_is_exist(self):
        response = Client().get('/authentication/register/')
        self.assertEqual(response.status_code, 200)

    def test_authentication_login_url_is_exist(self):
        response = Client().get('/authentication/')
        self.assertEqual(response.status_code, 200)

    def test_authentication_using_register_template(self):
        response = Client().get('/authentication/register/')
        self.assertTemplateUsed(response, 'authentication/register.html')

    def test_authentication_using_index_template(self):
        response = Client().get('/authentication/')
        self.assertTemplateUsed(response, 'authentication/login.html')

    def test_authentication_using_register_func(self):
        found = resolve('/authentication/register/')
        self.assertEqual(found.func, register_view)
    
    def test_authentication_using_index_func(self):
        found = resolve('/authentication/')
        self.assertEqual(found.func, login_view)

    # Model Test
    def test_model_can_create_account(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        count = Account.objects.count()
        self.assertEqual(1, count)
    
    def test_can_register(self):
        data = {
            "email":"test@gmail.com", 
            "nama_depan":"testdepan",
            "nama_belakang":"testbelakang",
            "username":"testing",
            "password":"password",
            "confirm_password":"password"
        }
        response = Client().post('/authentication/register/', data=data)
        
        count = Account.objects.count()

        self.assertEqual(1, count)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_can_login(self):
        new_account = Account.objects.create_user(
            email="test@gmail.com",
            username="test_user",
            nama_depan="testdepan",
            nama_belakang="testbelakang",
            password="password"
        )
        data = {
            "email":"test@gmail.com",
            "password":"password"
        }

        response = Client().post("/authentication/", data=data)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')