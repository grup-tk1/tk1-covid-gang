from .views import (
    login_view,
    register_view,
    logout_view
)
from django.urls import path

urlpatterns = [
    path('', login_view),
    path('register/', register_view),
    path('logout/', logout_view)
]