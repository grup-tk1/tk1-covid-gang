from django.shortcuts import render, redirect

from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
)

from .forms import UserLoginForm, UserRegisterForm

def login_view(request):
    if request.user.is_authenticated:
        return redirect("/")

    # Redirect user, hidden input, next
    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)
    
    if form.is_valid():
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        user = authenticate(email=email, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect('/')
    
    context = {
        'form':form,
    }

    return render(request, "authentication/login.html", context)

def register_view(request):
    if request.user.is_authenticated:
        return redirect("/")

    next = request.GET.get('next')
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False) # False means don't save it yet
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(email=user.email, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect('/')

    context = {
        'form':form,
    }
    return render(request, "authentication/register.html", context)

def logout_view(request):
    logout(request)
    return redirect('/')