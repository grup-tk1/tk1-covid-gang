from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import index
# Create your tests here.
class LandingPageTest(TestCase):
    def test_landing_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_landing_page_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing_page/index.html')
